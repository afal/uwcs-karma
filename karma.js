$(function () {
  var get_reasons = function (name) {
    var name = name || location.hash.replace(/^./, '');
    $.getJSON('karma.php', {reasons: name}, function (data) {
      var html = ich.reasons_list({reasons: data, name: name});
      $(html).modal({keyboard: true, show: true, backdrop: true});
    });
  };

  $.getJSON('karma.php', function (data) {
    $('#main').html(ich.karma_table({karmatrain: data}));
    $('table').tablesorter();
    $('.reasons').click(function () { get_reasons(this.innerHTML); });
    if (location.hash) {
      get_reasons();
    }
  });
});
