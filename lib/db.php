<?php
/**
 * Database abstraction class
 * 
 * @author Dafydd "Afal" Francis
 */
 
class db{
	private $host;
	private $username;
	private $password;
	private $database;
	private $query;

	public function __construct($host,$username,$password,$database){
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->database = $database;
	}
	
	public function connect(){
		mysql_connect($this->host,$this->username,$this->password) or die('Error: couldn\'t connect to database');
		mysql_select_db($this->database) or die("Could not select database. " . mysql_error());
	}
	
	public function query($query){
		$this->query = new dbIterator();
		$rows = mysql_query($query);
		if($rows===true)
			return $rows;
		while($row = mysql_fetch_assoc($rows))
			$this->query->add($row);
		return $this->query;
	}
	
	public function fetch(){
		return $this->query->fetch();
	}
}

class dbIterator implements Iterator
{
    private $var = array();

    public function __construct()
    {
        $this->var = array();
    }
	
	public function add($item){
		$this->var[] = $item;
	}

    public function rewind() {
        reset($this->var);
    }

    public function current() {
        $var = current($this->var);
        return $var;
    }

    public function key() {
        $var = key($this->var);
        return $var;
    }

    public function next() {
        $var = next($this->var);
        return $var;
    }

    public function valid() {
        $var = $this->current() !== false;
        return $var;
    }
	
	public function fetch(){
		$fetched = $this->current();
		$this->next();
		return $fetched;
	}
	
	public function dump() {
		return $this->var;
	}
}
