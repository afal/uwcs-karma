<?php
include_once 'db.php';

final class BlackJax{
	protected static $_instance;

	private function __construct(){
		session_start();
	}
	
	private function __clone(){}
	
	public static function run(){
		if( self::$_instance === NULL ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	public function get_db(){
		include_once 'config.php';
		$db = new db($host,$user,$pass,$database);
		$db->connect();
		return $db;
	}
}