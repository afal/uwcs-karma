<?php
include_once 'lib/blackjax.php';
$bj = BlackJax::run();
$db = $bj->get_db();

if (isset($_GET['reasons'])) {
    function dumpreasons($db, $mes, $in) {
        $query=$db->query(sprintf('SELECT `reason` FROM `_objectdb_plugins_karma_karmareasonobject` WHERE `string` = "%s" AND `direction` = %s', mysql_real_escape_string(stripslashes($_GET['reasons'])), $in));
        return array_map(function($res) { return $res['reason']; }, $query->dump());
    }

    $output = array(
        'gained' => dumpreasons($db, 'gained', 1),
        'lost' => dumpreasons($db, 'lost', -1),
        'not_changed' => dumpreasons($db, 'not changed', 0)
    );
} else {
    $result = $db->query('SELECT COUNT(reasons.string) AS reason_count, karma.string AS name, up, down, value AS total FROM _objectdb_plugins_karma_karmaobject AS karma LEFT JOIN _objectdb_plugins_karma_karmareasonobject AS reasons ON karma.string = reasons.string GROUP BY karma.string HAVING abs(total) > 3 ORDER BY total DESC, karma.string ASC ');

    $images=array();
    $handler = opendir('images');
    while ($file = readdir($handler))
        if ($file != '.' && $file != '..')
        	$images[strtolower(substr($file, 0, strpos($file, '.')))] = $file;
    closedir($handler);

    $rank = 0;
    $prevval = 0;
    $even=false;
    $output = array();

    foreach($result as $row) {
        if ($prevval != $row['total'])
            $rank++;
        $prevval = $row['total'];

        $row['rank'] = $rank;
        $row['image'] = isset($images[strtolower($row['name'])]) ? $images[strtolower($row['name'])] : '';

        $output[] = $row;
    }
}

header('Content-type: application/json');
echo json_encode($output);
